import setuptools

setuptools.setup(
    name="gitlab_pypi",
    version="0.0.2",
    author="Miguel Gattino",
    author_email="miguel.gattino@tno.nl",
    description="A simple python package hosted on GitLab, pip installable from "
                "GitLab.",
    packages=setuptools.find_packages(),
    install_requires=["numpy"],
    extras_require={
        "development": ["wheel", "twine"],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
