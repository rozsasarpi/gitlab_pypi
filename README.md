# gitlab_pypi

PyPI packages in the Package Registry

```python
from gitlab_pypi.approximate_pi import hit_and_miss

hit_and_miss(10_000)
```

## Building and sharing a package

Make a local package build:
```commandline
python setup.py sdist bdist_wheel
```

Check the build:
```commandline
twine check dist\<build file>
```


Official GitLab guide:

https://docs.gitlab.com/ee/user/packages/pypi_repository/