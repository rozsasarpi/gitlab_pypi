"""
This module contains methods to approximate pi.
"""

import numpy as np


def hit_and_miss(n_point):
    """
    Extremely efficient Monte Carlo based method to estimate pi.

    Args:
        n_point: number of points used for the Monte Carlo simulation.

    Returns:
        pi approximation.
    """
    point_xy = np.random.random((n_point, 2))
    point_r = np.sqrt(np.sum(point_xy ** 2, axis=1))
    return np.sum(point_r < 1) / n_point * 4
